import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent  {

  loading: boolean;

  constructor(private spotify: SpotifyService) { }
  artistas: any[] = [];
  buscar(termino) {
    this.loading = true;
    this.spotify.getArtistas(termino).subscribe((data: any) =>{
      console.log(data);
      this.loading = false;
      this.artistas = data;
    });
  }
}
