import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {

  }



  getQuery( query: string ) {
    const url = `https://api.spotify.com/v1/${query}`;
    const headers = new HttpHeaders({
      Authorization : 'Bearer BQDxdcQsXcpM-gfOaixmT2EZuApmKifCFqwlLNvA0U3zK0xyf4MffUc_KMQ1AZ-kz_xRMqg6pZy3pw9rh1Y'
    });
    return this.http.get(url, { headers });
  }

  getNewRelases() {

    return this.getQuery('browse/new-releases').pipe( map( data => data['albums'].items ));

  }

  getArtistas(termino) {

    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`).pipe( map( data =>  data['artists'].items ));

  }

  getArtista(id) {

    return this.getQuery(`artists/${ id }`);

  }

  getTopTracks(id: string) {

    return this.getQuery(`artists/${ id }/top-tracks?country=us`).pipe(map( data => data['tracks']));

  }

}

